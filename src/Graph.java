
public class Graph {
	Node[] nodes;
	int[][] edges;
	
	//Node class, to store the data
	private class Node {
		int cost;
		boolean known;
		int path;
		
		//initialize a node with default values
		private Node(){
			this.path  = -1;
			this.cost = 9999;
			known = false;
		}
	}
	
	//initialize a graph with numNode Nodes
	public Graph(int numNodes){
		nodes = new Node[numNodes];
		edges = new int[numNodes][numNodes]; //edges stored in an adjacency matrix
		
		for(int i = 0; i < numNodes; i++){
			nodes[i] = new Node();
			for(int j = 0; j < numNodes; j++){
				edges[i][j] = 0; //edge default set to 0 weight, i.e. no edge
			}
		}
	}
	
	//add and edge to the graph
	public void addEdge(int start, int end, int weight){
		edges[start][end] = weight;
	}
	
	//find the lowest cost node that is not known
	public int getLowest(){
		int lowest = 9999; //set lowest to some unreasonably high number
		int nodeIndex = -1;
		
		//check each node to see if it is the smallest
		for(int i = 0; i < nodes.length; i++){
			if(!nodes[i].known && nodes[i].cost < lowest){
				lowest = nodes[i].cost;
				nodeIndex = i;
			}
		}
		return nodeIndex;
	}
	
	//use dijkstra's algorithm to get cheapest paths from a start node (taken as that node's index)
	public void dijkstra(int start){
		//reset all nodes
		for(int i = 0; i < nodes.length; i++){
			nodes[i].known = false; 
			nodes[i].cost = 9999; // set cost to some unreasonably high number
		}
		
		nodes[start].cost = 0; //set starting node to 0
		
		//until we have no more nodes...
		for(int i = 0; i < nodes.length; i++){
			//print out the current state of affairs
			System.out.println("Step " + (i + 1));
			for(int j = 0; j < nodes.length; j++){
				System.out.println("Node: " + j + " Cost: " + nodes[j].cost + " Path: " + nodes[j].path);
			}
			System.out.println("-----------------------");
			
			//get the lowest cost node
			int lowestCost = getLowest(); 
			nodes[lowestCost].known = true;
			
			//find all outgoing edges
			for(int j = 0; j < nodes.length; j++){
				//if there is an outgoing edge and the target node is not known...
				if(edges[lowestCost][j] > 0 && !nodes[j].known){
					//if the cost of the target node would be smaller using this edge set the cost
					if(nodes[lowestCost].cost + edges[lowestCost][j] < nodes[j].cost){
						nodes[j].cost = nodes[lowestCost].cost + edges[lowestCost][j];
						nodes[j].path = lowestCost;
					}
				}
			}
		}
		
		//print out path information for each node
		for(int i = 0; i < nodes.length; i++){
			System.out.println("Node: "+ i);
			System.out.println("Cost: " + nodes[i].cost);
			
			String reversePath = "";
			String path = "";
			int cur = i;
			
			//get the path (in reverse)
			while(nodes[cur].path > -1){
				reversePath += nodes[cur].path;
				cur = nodes[cur].path;
				if(nodes[cur].path > -1){
					reversePath += ">-";
				}
			}
			
			//fix path direction
			for(int j = reversePath.length() - 1; j >= 0 ; j--){
				path += reversePath.charAt(j);
			}
			
			//check for root
			if(path.length() == 0){
				path = "root";
			}else{
				path += "->this";
			}
			
			System.out.println("Path: " + path);
			System.out.println("--------------");
		}
	}
}