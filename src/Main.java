
public class Main {
	public static void main(String[] args) {
		Graph g = new Graph(7);
		g.addEdge(0,1,5);
		g.addEdge(0,2,3);
		g.addEdge(1,2,2);
		g.addEdge(1,6,1);
		g.addEdge(1,4,3);
		g.addEdge(2,3,7);
		g.addEdge(2,4,7);
		g.addEdge(3,5,6);
		g.addEdge(3,0,2);
		g.addEdge(4,5,1);
		g.addEdge(4,3,2);
		g.addEdge(6,4,1);
		
		g.dijkstra(0);
	}
}
